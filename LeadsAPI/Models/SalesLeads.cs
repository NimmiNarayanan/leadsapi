﻿using System;
using System.Collections.Generic;

namespace LeadsAPI.Models
{
    public partial class SalesLeads
    {
        public int Salesleadid { get; set; }
        public string Subject { get; set; }
        public string Companyname { get; set; }
        public string Jobtitle { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Mobilephone { get; set; }
        public string Emailaddress1 { get; set; }
        public int? Statuscode { get; set; }
        public int? Leadsourcecode { get; set; }
        public int? Leadowner { get; set; }
        public string Description { get; set; }
        public string Address1City { get; set; }
        public string Address1Country { get; set; }
        public string Address1County { get; set; }
        public string Address1Name { get; set; }
        public string Address1Line1 { get; set; }
        public string Address1Line2 { get; set; }
        public string Address1Line3 { get; set; }
        public string Address1Telephone1 { get; set; }
        public string Address1Telephone2 { get; set; }
        public decimal? Budgetamount { get; set; }
        public int? Budgetstatus { get; set; }
        public string Budgetcurrency { get; set; }
        public string Confirminterest { get; set; }
        public string Decisionmaker { get; set; }
        public DateTime? Lastcontacteddatetime { get; set; }
        public string MsdynGdproptout { get; set; }
        public DateTime? CreationDatetime { get; set; }
        public string Companyid { get; set; }
        public string Contactid { get; set; }
    }
}
