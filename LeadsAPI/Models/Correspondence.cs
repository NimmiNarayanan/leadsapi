﻿using System;
using System.Collections.Generic;

namespace LeadsAPI.Models
{
    public partial class Correspondence
    {
        public int? Salesleadid { get; set; }
        public int? Userid { get; set; }
        public string Email { get; set; }
        public DateTime? Datetimestamp { get; set; }

        public virtual SalesLeads Saleslead { get; set; }
        public virtual Users User { get; set; }
    }
}
