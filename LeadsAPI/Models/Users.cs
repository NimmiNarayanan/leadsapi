﻿using System;
using System.Collections.Generic;

namespace LeadsAPI.Models
{
    public partial class Users
    {
        public Users()
        {
            SalesLeads = new HashSet<SalesLeads>();
        }

        public int Userid { get; set; }
        public string Samaccountname { get; set; }
        public string Emailaddress { get; set; }
        public string Employeeid { get; set; }
        public string Issalesperson { get; set; }
        public string Isaccountmanager { get; set; }
        public string Isadministrator { get; set; }

        public virtual ICollection<SalesLeads> SalesLeads { get; set; }
    }
}
