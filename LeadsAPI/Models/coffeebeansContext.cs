﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LeadsAPI.Models
{
    public partial class coffeebeansContext : DbContext
    {
        public coffeebeansContext()
        {
        }

        public coffeebeansContext(DbContextOptions<coffeebeansContext> options)
            : base(options)
        {
        }

        public virtual DbSet<SalesLeads> SalesLeads { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=tcp:lugh.database.windows.net,1433;Initial Catalog=coffeebeans;Persist Security Info=False;User ID=g1user;Password=stubborn_k1w1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SalesLeads>(entity =>
            {
                entity.HasKey(e => e.Salesleadid)
                    .HasName("PK__SalesLea__89FB9A9AF20B4FEF");

                entity.Property(e => e.Salesleadid).HasColumnName("salesleadid");

                entity.Property(e => e.Address1City)
                    .HasColumnName("address1_city")
                    .HasMaxLength(30);

                entity.Property(e => e.Address1Country)
                    .HasColumnName("address1_country")
                    .HasMaxLength(3);

                entity.Property(e => e.Address1County)
                    .HasColumnName("address1_county")
                    .HasMaxLength(30);

                entity.Property(e => e.Address1Line1)
                    .HasColumnName("address1_line1")
                    .HasMaxLength(30);

                entity.Property(e => e.Address1Line2)
                    .HasColumnName("address1_line2")
                    .HasMaxLength(30);

                entity.Property(e => e.Address1Line3)
                    .HasColumnName("address1_line3")
                    .HasMaxLength(30);

                entity.Property(e => e.Address1Name)
                    .HasColumnName("address1_name")
                    .HasMaxLength(30);

                entity.Property(e => e.Address1Telephone1)
                    .HasColumnName("address1_telephone1")
                    .HasMaxLength(24);

                entity.Property(e => e.Address1Telephone2)
                    .HasColumnName("address1_telephone2")
                    .HasMaxLength(24);

                entity.Property(e => e.Budgetamount)
                    .HasColumnName("budgetamount")
                    .HasColumnType("decimal(18, 6)");

                entity.Property(e => e.Budgetcurrency)
                    .HasColumnName("budgetcurrency")
                    .HasMaxLength(3);

                entity.Property(e => e.Budgetstatus).HasColumnName("budgetstatus");

                entity.Property(e => e.Companyid)
                    .HasColumnName("companyid")
                    .HasMaxLength(10);

                entity.Property(e => e.Companyname)
                    .HasColumnName("companyname")
                    .HasMaxLength(50);

                entity.Property(e => e.Confirminterest)
                    .HasColumnName("confirminterest")
                    .HasMaxLength(1);

                entity.Property(e => e.Contactid)
                    .HasColumnName("contactid")
                    .HasMaxLength(10);

                entity.Property(e => e.CreationDatetime)
                    .HasColumnName("creationDatetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Decisionmaker)
                    .HasColumnName("decisionmaker")
                    .HasMaxLength(1);

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Emailaddress1)
                    .HasColumnName("emailaddress1")
                    .HasMaxLength(60);

                entity.Property(e => e.Firstname)
                    .HasColumnName("firstname")
                    .HasMaxLength(30);

                entity.Property(e => e.Jobtitle)
                    .HasColumnName("jobtitle")
                    .HasMaxLength(50);

                entity.Property(e => e.Lastcontacteddatetime)
                    .HasColumnName("lastcontacteddatetime")
                    .HasColumnType("date");

                entity.Property(e => e.Lastname)
                    .HasColumnName("lastname")
                    .HasMaxLength(30);

                entity.Property(e => e.Leadowner).HasColumnName("leadowner");

                entity.Property(e => e.Leadsourcecode).HasColumnName("leadsourcecode");

                entity.Property(e => e.Mobilephone)
                    .HasColumnName("mobilephone")
                    .HasMaxLength(24);

                entity.Property(e => e.MsdynGdproptout)
                    .HasColumnName("msdyn_gdproptout")
                    .HasMaxLength(1);

                entity.Property(e => e.Statuscode).HasColumnName("statuscode");

                entity.Property(e => e.Subject)
                    .HasColumnName("subject")
                    .HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
