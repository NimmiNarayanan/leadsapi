﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LeadsAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace LeadsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LeadsController : ControllerBase
    {
        private readonly coffeebeansContext _context;

        public LeadsController(coffeebeansContext context)
        {
            _context = context;
        }


        // GET: api/Leads
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SalesLeads>>> GetLeads()
        {
            return await _context.SalesLeads.ToListAsync();
        }

        // GET: api/Leads/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<ActionResult<SalesLeads>> GetLeadById(int id)
        {
            var lead = await _context.SalesLeads.FindAsync(id);

            if (lead == null)
            {
                return NotFound();
            }

            return lead;
        }

        // POST: api/Leads
        [HttpPost]
        public async Task<ActionResult<SalesLeads>> Post(SalesLeads lead)
        {
            _context.SalesLeads.Add(lead);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLeadById", new { id = lead.Salesleadid }, lead);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<SalesLeads>> DeleteLead(int id)
        {
            var lead = await _context.SalesLeads.FindAsync(id);
            if (lead == null)
            {
                return NotFound();
            }

            _context.SalesLeads.Remove(lead);
            await _context.SaveChangesAsync();

            return lead;
        }
    }
}
